import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CadastroGestor {

	public static void main(String[] args) throws InterruptedException {
		// Declara��o e instancia��o de objetos e vari�veis
		// Comentar as linhas abaixo/acima conforme o navegador utilizado
		// System.setProperty("webdriver.gecko.driver","C:\\Users\\diego.soares\\Documents\\workspace\\geckodriver.exe");
		// WebDriver driver = new FirefoxDriver();
		// Comentar as linhas abaixo/acima conforme o navegador utilizado
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\diego.soares\\Documents\\workspace\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		// driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

		String baseUrl = "http://suphpts02.mds.net/adesan";
		String expectedTitle = "ADESAN (Sistema de Ades�o ao SISAN)";
		String actualTitle = "";

		// Inicia o navegador e navega at� a URL informada
		driver.get(baseUrl);

		// Armazena o valor do t�tulo da p�gina
		actualTitle = driver.getTitle();

		/*
		 * Compara o t�tulo apresentado na p�gina com o t�tulo esperado e apresenta o
		 * resultado
		 */
		if (actualTitle.contentEquals(expectedTitle)) {
			System.out.println("T�tulo da p�gina: OK");
		} else {
			System.out.println("T�tulo da p�gina: Falhou");
		}

		driver.findElement(By.name("loginSisan")).sendKeys("26039865032");
		driver.findElement(By.name("senhaSisan")).sendKeys("26845252");

		WebDriverWait wait = new WebDriverWait(driver, 3000);

		wait.until(ExpectedConditions.elementToBeClickable(By.name("btacao")));
		driver.findElement(By.name("btacao")).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("novo")));
		driver.findElement(By.id("novo")).click();

		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("TB_iframeContent"));

		new Select(driver.findElement(By.id("co_tipo_perfil"))).selectByVisibleText("CAISAN Estadual");
		driver.findElement(By.id("no_email")).sendKeys("andrepereira@email.com");
		driver.findElement(By.id("nu_telefone")).sendKeys("11989697320");
		driver.findElement(By.id("ds_senha")).sendKeys("123456");
		driver.findElement(By.id("ds_senha_confirmar")).sendKeys("123456");
		new Select(driver.findElement(By.id("co_uf"))).selectByVisibleText("SP");
		driver.findElement(By.id("ds_funcao")).sendKeys("Assistente");
		driver.findElement(By.id("ds_setor")).sendKeys("Administrativo");
		driver.findElement(By.id("no_pessoa")).sendKeys("Andr� Pereira");
		driver.findElement(By.id("nu_cpf")).sendKeys("67487960080");

		wait.until(ExpectedConditions.elementToBeClickable(By.id("Enviar")));
		driver.findElement(By.id("Enviar")).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-button-text")));
		driver.findElement(By.className("ui-button-text")).click();

		/*
		 * wait.until(ExpectedConditions.elementToBeClickable(By.id("Enviar")));
		 * driver.findElement(By.id("Enviar")).click();
		 * 
		 * 
		 * driver.findElement(By.id("cancelar")).click();
		 * 
		 * driver.switchTo().defaultContent();
		 * 
		 * driver.findElement(By.name("no_pessoa")).sendKeys("Amanda Cardoso"); //
		 * wait.until(ExpectedConditions.elementToBeClickable(By.id("consultar")));
		 * Thread.sleep(1000); driver.findElement(By.id("consultar")).click();
		 * 
		 * // Fecha o navegador driver.quit();
		 */
	}
}
