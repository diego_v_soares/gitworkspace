package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PaginaBase {

	WebDriver driver;

	public PaginaBase(WebDriver driver) {
		this.driver = driver;
	}

	public PaginaBase() {
		System.out.println("Abrindo navegador....");
		this.driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	public void navegateTo(String url) {
		driver.navigate().to(url);
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void fechaNavegador() {
		System.out.println("Fechando navegador.....");
		getDriver().quit();
	}
}