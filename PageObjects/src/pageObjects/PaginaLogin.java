package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaginaLogin extends PaginaBase {

	WebDriverWait wait = new WebDriverWait(driver, 30);

	public PaginaLogin(WebDriver driver) {
		super(driver);
	}

	public String obterTextoPagina() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("titulo")));
		return getDriver().findElement(By.id("titulo")).getText();
	}

	public PaginaInicial logar(String cpf, String senha) {
		realizaLogin(cpf, senha);
		return new PaginaInicial(getDriver());
	}

	private void realizaLogin(String cpf, String senha) {
		getDriver().findElement(By.name("loginSisan")).clear();
		getDriver().findElement(By.name("loginSisan")).sendKeys(cpf);
		getDriver().findElement(By.name("senhaSisan")).clear();
		getDriver().findElement(By.name("senhaSisan")).sendKeys(senha);

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("btacao")));
		getDriver().findElement(By.name("btacao")).click();
	}

}