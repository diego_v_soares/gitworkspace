package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaginaInicial extends PaginaBase {

	WebDriverWait wait = new WebDriverWait(driver, 30);

	public PaginaInicial(WebDriver driver) {
		super(driver);
	}

	public String obterTextoPagina() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("h2")));
		return getDriver().findElement(By.tagName("h2")).getText();
	}
	
	public PaginaCadastro acessaPaginaDeCadastro() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("novo")));
		getDriver().findElement(By.id("novo")).click();
		return new PaginaCadastro(getDriver());
	}

	public PaginaInicial consultar(String nome) throws InterruptedException {
		consultaGestor(nome);
		return new PaginaInicial(getDriver());
	}

	public void consultaGestor(String nome) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("no_pessoa")));
		getDriver().findElement(By.name("no_pessoa")).sendKeys(nome);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("consultar")));
		getDriver().findElement(By.id("consultar")).click();

	}
}