package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaginaCadastro extends PaginaBase {

	WebDriverWait wait = new WebDriverWait(driver, 30);

	public PaginaCadastro(WebDriver driver) {
		super(driver);
	}

	public PaginaInicial cadastrar(String tipoPerfil, String email, String telefone, String senha, String confirmaSenha,
			String uf, String funcao, String setor, String nome, String cpf) {
		preencherCadastro(tipoPerfil, email, telefone, senha, confirmaSenha, uf, funcao, setor, nome, cpf);
		return new PaginaInicial(getDriver());
	}

	public String obterTextoPagina() {
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("TB_iframeContent"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("h1")));
		return getDriver().findElement(By.tagName("h1")).getText();
	}

	public void preencherCadastro(String tipoPerfil, String email, String telefone, String senha, String confirmaSenha,
			String uf, String funcao, String setor, String nome, String cpf) {

		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 3000);
		 * wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(
		 * "TB_iframeContent"));
		 */

		new Select(getDriver().findElement(By.id("co_tipo_perfil"))).selectByVisibleText(tipoPerfil);
		getDriver().findElement(By.id("no_email")).sendKeys(email);
		getDriver().findElement(By.id("nu_telefone")).sendKeys(telefone);
		getDriver().findElement(By.id("ds_senha")).sendKeys(senha);
		getDriver().findElement(By.id("ds_senha_confirmar")).sendKeys(confirmaSenha);
		new Select(getDriver().findElement(By.id("co_uf"))).selectByVisibleText(uf);
		getDriver().findElement(By.id("ds_funcao")).sendKeys(funcao);
		getDriver().findElement(By.id("ds_setor")).sendKeys(setor);
		getDriver().findElement(By.id("no_pessoa")).sendKeys(nome);
		getDriver().findElement(By.id("nu_cpf")).sendKeys(cpf);

		wait.until(ExpectedConditions.elementToBeClickable(By.id("Enviar")));
		getDriver().findElement(By.id("Enviar")).click();
		// getDriver().findElement(By.id("cancelar")).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-button-text")));
		driver.findElement(By.className("ui-button-text")).click();

		/*
		 * wait.until(ExpectedConditions.elementToBeClickable(By.id("Enviar")));
		 * getDriver().findElement(By.id("Enviar")).click();
		 * 
		 * getDriver().switchTo().defaultContent();
		 */
	}
}