package funcionalTests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pageObjects.*;

public class CadastraGestorTest {

	protected PaginaBase paginaBase = new PaginaBase();
	protected PaginaLogin paginaLogin;
	protected PaginaInicial paginaInicial;
	protected PaginaCadastro paginaCadastro;

	@Before
	public void acessandoPaginaLogin() {
		this.paginaBase.navegateTo("http://suphpts02.mds.net/adesan");
	}

	@Test
	public void cadastraGestor() throws Exception {
		adotandoQueUsuarioLogado();
		acessaCadastro();
		preencheCadastro();
		// consultaGestor();
		fechaNavegador();
	}

	private void adotandoQueUsuarioLogado() {
		this.paginaLogin = new PaginaLogin(this.paginaBase.getDriver());
		Assert.assertEquals("O SISAN", this.paginaLogin.obterTextoPagina());
		this.paginaInicial = paginaLogin.logar("26039865032", "26845252");
	}

	private void acessaCadastro() throws InterruptedException {
		Assert.assertEquals("Consultar Gestores Cadastrados", this.paginaInicial.obterTextoPagina());
		this.paginaCadastro = paginaInicial.acessaPaginaDeCadastro();
	}

	private void preencheCadastro() {
		this.paginaLogin = new PaginaLogin(this.paginaBase.getDriver());
		Assert.assertEquals("Cadastrar Usu�rio", this.paginaCadastro.obterTextoPagina());
		this.paginaInicial = paginaCadastro.cadastrar("CAISAN Estadual", "romariofaria@email.com", "21985542120",
				"123456", "123456", "RJ", "Assessor", "Jur�dico", "Romario Faria", "81125458046");
	}

	/*
	 * private void consultaGestor() throws InterruptedException { this.paginaLogin
	 * = new PaginaLogin(this.paginaBase.getDriver()); this.paginaInicial =
	 * paginaInicial.consultar("Cristiano Ronaldo"); }
	 */

	@After
	public void fechaNavegador() {
		this.paginaBase.fechaNavegador();
	}

}
